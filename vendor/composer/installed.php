<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'drupal-custom-module',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '9a9025f766acb60d6fb061f3fff116ab2d1f1100',
        'name' => 'studioroger/cache-clear-on-cron',
        'dev' => true,
    ),
    'versions' => array(
        'composer/installers' => array(
            'pretty_version' => '1.x-dev',
            'version' => '1.9999999.9999999.9999999-dev',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/./installers',
            'aliases' => array(),
            'reference' => '894a0b5c5d34c88b69b097f2aae1439730fa6836',
            'dev_requirement' => false,
        ),
        'roundcube/plugin-installer' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'shama/baton' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'studioroger/cache-clear-on-cron' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'drupal-custom-module',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '9a9025f766acb60d6fb061f3fff116ab2d1f1100',
            'dev_requirement' => false,
        ),
    ),
);
